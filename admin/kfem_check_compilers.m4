dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:    27-Apr-99 at 11:25:05
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl DESCRIP-END.

AC_DEFUN(AC_BZ_SET_COMPILER,
  [cxxwith=`echo $1 | sed -e 's/ /@/g'`
   case "$cxxwith" in
     *:*@*)                 # Full initialization syntax
       CXX=`echo "$cxxwith" | sed  -n -e 's/.*:\(.*\)@.*/\1/p'`
       CXXFLAGS=`echo "$cxxwith" | sed  -n -e 's/.*:.*@\(.*\)/\1/p'`
     ;;
     *:*)                   # Simple initialization syntax
       CXX=`echo "$cxxwith" | sed  -n -e 's/.*:\(.*\)/\1/p'`
       CXXFLAGS=$3
     ;;
     *)                     # Default values
       CXX=$2
       CXXFLAGS=$3
       echo "$CXX $CXXFLAGS"	
     ;;
   esac])

AC_DEFUN(KFEM_CHECK_COMPILERS,
[
AC_SUBST(ARFLAGS)
AC_SUBST(AR)
AC_SUBST(CFLAGS)
AC_SUBST(RANLIB)

known_compiler=0
ARFLAGS="cru"
AC_ARG_WITH(cxx,
  [  --with-cxx=COMPILER[:name-flags]     set options for COMPILER (KCC, cray, egcs, DECcxx, aCC)],
  [case "$withval" in
     KCC*)               # KAI C++  http://www.kai.com/
       echo "Configuring for KAI C++"
       bz_compiler=kcc
       AC_BZ_SET_COMPILER($withval,"KCC","--one_instantiation_per_object --no_exceptions --restrict")
       CXX_OPTIMIZE_FLAGS="+K3 -O3 --inline_keyword_space_time=10000 --abstract_float --abstract_pointer"
       CXX_DEBUG_FLAGS="-g +K1 -O2 -DDEBUG=1 -DBZ_DEBUG"
       F77=f77
       F77_OPTIMIZE_FLAGS="-O2 -funroll-loops"
       CFLAGS="\${CXXFLAGS}"
       AR="KCC  \${CXXFLAGS}"
       ARFLAGS="-o"
       RANLIB="@echo"
     ;;
     insure*)
       echo "Configuring for insure (wahoo!)"
       bz_compiler=egcs
       AC_BZ_SET_COMPILER($withval,"insure","-ftemplate-depth-30")
       CXX_OPTIMIZE_FLAGS="-ftemplate-depth-30-g -O "
       CXX_DEBUG_FLAGS="-g -DBZ_DEBUG"
       F77=g77
       F77_OPTIMIZE_FLAGS="-O2 -funroll-loops"
       CXXFFLAGS="-DBZ_FORTRAN_SYMBOLS_WITH_DOUBLE_TRAILING_UNDERSCORES"
	;;
     g++*)              # EGCS  http://egcs.cygnus.com/
       echo "Configuring for EGCS (wahoo!)"
       bz_compiler=egcs
       AC_BZ_SET_COMPILER($withval,"g++","-ftemplate-depth-30")
       CXX_OPTIMIZE_FLAGS="-O3 -fstrength-reduce -frerun-loop-opt -fgcse -funroll-loops -fstrict-aliasing -fomit-frame-pointer -fexpensive-optimizations -ffast-math"
#       CXX_DEBUG_FLAGS="-W -Wall -Wmissing-prototypes -Wstrict-prototypes -Wtraditional -Wconversion -Wpointer-arith -Wcast-qual -Wcast-align -Wwrite-strings -fshort-enums -fno-common -Wnested-externs -g -O2 -fno-inline "
       CXX_DEBUG_FLAGS="-W -Wall -g -O0 -DDEBUG=1"
       CXX_PROF_FLAGS="-g -pg"
       F77=g77
       F77_OPTIMIZE_FLAGS="-O2 -funroll-loops"
       CXXFFLAGS="-DBZ_FORTRAN_SYMBOLS_WITH_DOUBLE_TRAILING_UNDERSCORES"
       AR="ar"
       RANLIB="ranlib"
     ;;
     eg++*)              # EGCS  http://egcs.cygnus.com/
       echo "Configuring for EGCS (wahoo!)"
       bz_compiler=egcs
       AC_BZ_SET_COMPILER($withval,"eg++","-ftemplate-depth-30")
       CXX_OPTIMIZE_FLAGS="-O2 -funroll-loops -fstrict-aliasing "
#       CXX_DEBUG_FLAGS="-W -Wmissing-prototypes -Wstrict-prototypes -Wtraditional -Wconversion -Wpointer-arith -Wcast-qual -Wcast-align -Wwrite-strings -fshort-enums -fno-common -Wnested-externs -g -O2"
       F77=g77
       F77_OPTIMIZE_FLAGS="-O2 -funroll-loops"
       CXXFFLAGS="-DBZ_FORTRAN_SYMBOLS_WITH_DOUBLE_TRAILING_UNDERSCORES"
       AR="ar"
       RANLIB="ranlib"
     ;;
     pgCC*)
	echo "Configuring for Portland Group"
       bz_compiler=pgCC
       AC_BZ_SET_COMPILER($withval,"pgCC","")
#       CXX_OPTIMIZE_FLAGS="-fast -pc 64 -Mconcour -Mnoframe -Mnodepchk -Minline=levels:25"
       CXX_OPTIMIZE_FLAGS="-O3 -Mcache_align -Minline=levels:25"
       CXX_DEBUG_FLAGS="-g   -DDEBUG=1"
       CXX_PROF_FLAGS="-g -pg"
       F77=g77
       F77_OPTIMIZE_FLAGS="-fast -pc 64 -Mconcour "
       CXXFFLAGS="-DBZ_FORTRAN_SYMBOLS_WITH_DOUBLE_TRAILING_UNDERSCORES"
       AR="ar"
       RANLIB="ranlib"
      export GXX=pgCC
	;;
     aCC*)
       echo "Configuring for aCC (HP-UX)"
       bz_compiler=aCC
       AC_BZ_SET_COMPILER($withval,"aCC","-Wc,-fansi_for_scope,on")
       CXX_OPTIMIZE_FLAGS="+O2"
       CXX_DEBUG_FLAGS="-g"
       F77=f77
       F77_OPTIMIZE_FLAGS="-O"
     ;;
     cray*)
       echo "Configuring for Cray C++."
       bz_compiler=cray
       AC_BZ_SET_COMPILER($withval,"CC","-h instantiate=used -h noexceptions -h new_for_init")
       CPPFLAGS="$CPPFLAGS -DCRAY_T3E"
#       CXX_OPTIMIZE_FLAGS="-O3 -hpipeline3 -hunroll -haggress -hscalar2"
       CXX_DEBUG_FLAGS="-g"
       AR="ar"
       RANLIB="ranlib"
     ;;	
     *)
        AC_MSG_WARN([--with-cxx: invalid option $withval])
     ;;
   esac
   know_compiler=1 ])

if test $know_compiler -eq 0; then
cat << 'EOH'

This script only recognizes a small set of compilers and platforms so far.
You will have to rerun this script with one of these options:
  --with-cxx=KCC    KAI C++ (http://www.kai.com/)
  --with-cxx=egcs   EGCS (http://egcs.cygnus.com/)
  --with-cxx=cray   Cray C++ 3.0 or better (http://www.cray.com/)
  --with-cxx=DECcxx DEC cxx 6.10 or better (http://www.dec.com/)

The Intel C++ compiler is also able to compile blitz, but this
script doesn't handle it yet.  Ask if you need it.

EOH
exit 1
fi




bz_optimize=false
AC_ARG_ENABLE(optimize,
  [  --enable-optimize       enable compiler optimization flags],
  [echo "$CXX"	
#   case "$CXX" in
#	g++*)
	   if test "$enableval" = yes; then
	     case "$host" in 
		hppa*)
		# -mpa-risc-2-0 -march=2.0
#		CXX_OPTIMZE_ARCH_FLAGS="-mpa-risc-2-0 -march=2.0 -mschedule=8000"
		;;
		i686*)
		if test "$with_cxx" = g++; then
		CXX_OPTIMZE_ARCH_FLAGS="-mcpu=pentiumpro -march=pentiumpro"
		fi
		;;
		alphaev56*)
		CXX_OPTIMZE_ARCH_FLAGS="-mcpu=ev56 "
		;;
		cray*)
		CPPFLAGS="$CPPFLAGS -DCRAY_T3E"
#		CXX_OPTIMZE_ARCH_FLAGS="-h inline1 -h scalar3 -h jump -hpipeline1 "
                if test "$with_cxx" = KCC; then
		F77=f90
		F77FLAGS="-O,aggress,pipeline3,unroll2,scalar3,inline3"
		CXX_OPTIMZE_ARCH_FLAGS="--backend -hpipeline3 --backend -hunroll --backend -haggress --backend -hscalar2"
                fi;;
	     esac
	#;;
     #esac
     echo "Optimization flags enabled"
     CXXFLAGS="$CXXFLAGS $CXX_OPTIMIZE_FLAGS $CXX_OPTIMZE_ARCH_FLAGS"
     F77FLAGS="$F77_OPTIMIZE_FLAGS $F77_OPTIMIZE_ARCH_FLAGS"	
   fi], [])

bz_debug=false
AC_ARG_ENABLE(debug,
  [  --enable-debug          enable compiler debug flags],
  [if test "$enableval" = yes; then
     echo "Debugging flags enabled"
     CXXFLAGS="$CXXFLAGS $CXX_DEBUG_FLAGS"
     CPPFLAGS="$CPPFLAGS -DALL_ASSERTIONS=1"
	#     AC_CHECK_LIB(efence,EF_Exit,ac_define_efence=1)
   fi], [])


bz_prof=false
AC_ARG_ENABLE(profile,
  [  --enable-profile          enable compiler debug flags],
  [if test "$enableval" = yes; then
     echo "Profiling flags enabled"
     CXXFLAGS="$CXXFLAGS $CXX_PROF_FLAGS"
	#     AC_CHECK_LIB(efence,EF_Exit,ac_define_efence=1)
   fi], [])
])

