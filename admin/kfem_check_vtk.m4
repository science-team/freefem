dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:     8-Jun-00 at 17:25:15
dnl LAST-MOD:     13-Jun-00 at 17:50:02 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl DESCRIP-END.

AC_DEFUN(KFEM_CHECK_VTK,
[AC_MSG_CHECKING([vtk])
 ac_vtk_include=vtk.h
 ac_vtk_lib=libVTKCommon.so 

 for ac_dir in \
  /usr/local/ \
  ;
 do
   if test -r "$ac_dir/include/vtk/$ac_vtk_include"; then
    ac_vtk_includes=$ac_dir/include/vtk
    if test -r "$ac_dir/lib/$ac_vtk_lib"; then
     ac_vtk_libs=$ac_dir/lib/
     break
    fi
    break
   fi
 done
 if test "$ac_vtk_libs" = "" -o "$ac_vtk_includes" = "";then
   AC_MSG_RESULT([no])
 fi 
 AC_MSG_RESULT([yes])
 CPPFLAGS="${CPPFLAGS} -I$ac_vtk_includes "
 if ! test "$ac_vtk_libs" = ""; then
   LDFLAGS="${LDFLAGS} -L$ac_vtk_libs"
 fi 
 VTKLIBS="-lVTKPatented -lVTKContrib -lVTKGraphics -lVTKImaging -lVTKCommon"
 AC_SUBST(VTKLIBS)
 ]
 AC_DEFINE(HAVE_VTK))
