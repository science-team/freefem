pi:=4*atan(1);
border(1,0,2,17)
{x:= cos(pi*t); y:= sin(pi*t)};
border(0,-1,1,7) { x:= t; y:=0; };
border(0,0,1,4) { x:=0;y:=t};
buildmesh(300);

solve(v) {
onbdy(1) id(v)*region+dnu(v)=0;
pde(v)
  -laplace(v)*(1+region) =1;
};
plot(v);
