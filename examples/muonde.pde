complex;
a:=20; b:=20; c:=15; d:=8;
border(1,0,6,81){
if(t<=1)then {x:=a*t; y:=0};
if((t>1)and(t<=2)) then {x:=a; y:= b*(t-1)};
if((t>2)and(t<=3)) then { x:=a*(3-t);y:=b};
if((t>3)and(t<=4))then {x:=0;y:=b-(b-c)*(t-3)};
if((t>4)and(t<=5)) then {x:=0; y:=c-(c-d)*(t-4); ib:=2};
if(t>5) then { x:=0; y:= d*(6-t)};
};
e:=2; l:=12; f:=2; g:=2;
border(3,0,8,61){
if(t<=1) then {x:=a-f+e*(t-1); y:=g};
if((t>1)and(t<=4)) then {x:=a-f; y:=g+l*(t-1)/3};
if((t>4)and(t<=5)) then {x:=a-f-e*(t-4); y:=l+g};
if(t>5) then {x:=a-e-f; y:= l+g-l*(t-5)/3};
};
buildmesh(1500);
savemesh('muonde.msh');
solve(v) begin
   onbdy(1) v=0;
   onbdy(2) v=sin(pi*(y-c)/(c-d));
pde(v) id(v)*(1+region)+laplace(v)*(1-0.5*I)=0;
end;  

plot(v); plot(Im(v));       save('v.dta',v);
f=(v*v + Im(v)*Im(v))*region;  save('f.dta',f);
plot(f);

solve(temp) begin 
      onbdy(1,2)temp=0;
      pde(temp) -laplace(temp)=f;
end;  plot(temp); save('temp.dta',temp);
