h:=.009;
alpha:=atan(1);
l:=h/tan(alpha);
border(1,0,1,60)   begin x:= l*t;  y := h * t          end;
border(1,0,1,60)   begin x:= l*(1-t);  y := h     end;
border(1,0,1,60)   begin x:= 0;   y := h*(1-t) end;
buildmesh(800);


solve(v){
  onbdy(1) v=0;
  pde(v) -laplace(v) = 1;
};
plot(v);

