wait;
n:=15;
border(1,0,1, n){x:=t;y:=0};
border(1,0,1, n){x:=1;y:=t};
border(1,0,1, n){x:=1-t;y:=1};
border(1,0,1, n){x:=0;y:=1-t};  
buildmesh(1800);
f=-2*(x*(x-1)+y*(y-1));
varsolve(u,v;w,s)
begin
g = dx(u)*dx(w)+dy(u)*dy(w)+dx(v)*dx(s)+dy(v)*dy(s);
h = w+s;
onbdy(1) v = x;
 onbdy(1) u = x;
end : intt[g]  - intt[f,h] ;
plot(v);
plot(u-v);
w = x+x*y*(y-1)*(x-1);
plot(w-v);
